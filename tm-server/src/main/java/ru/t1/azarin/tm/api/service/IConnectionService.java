package ru.t1.azarin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    Connection getConnection();

}
