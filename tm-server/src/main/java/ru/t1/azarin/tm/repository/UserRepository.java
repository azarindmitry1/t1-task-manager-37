package ru.t1.azarin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    private final String tableName = "tm_user";

    @NotNull
    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setFirstName(row.getString("first_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setLastName(row.getString("last_name"));
        user.setLocked(row.getBoolean("locked"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setEmail(row.getString("email"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password_hash, first_name, middle_name, last_name, locked, role, email) " +
                        "VALUES (?, ?, ? , ?, ?, ?, ?, ?, ?)", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getLastName());
            statement.setBoolean(7, user.isLocked());
            statement.setString(8, user.getRole().toString());
            statement.setString(9, user.getEmail());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET login = ?, password_hash = ?, first_name = ?, middle_name = ?, last_name = ?, " +
                        "locked = ?, role = ?, email = ? WHERE id = ?", getTableName()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPasswordHash());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getMiddleName());
            preparedStatement.setString(5, user.getLastName());
            preparedStatement.setBoolean(6, user.isLocked());
            preparedStatement.setString(7, user.getRole().toString());
            preparedStatement.setString(8, user.getEmail());
            preparedStatement.setString(9, user.getId());
            preparedStatement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, first_name, middle_name, last_name, locked, role, email " +
                        "FROM %s WHERE login = ? LIMIT 1", getTableName()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, login);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, first_name, middle_name, last_name, locked, role, email " +
                        "FROM %s WHERE email = ? LIMIT 1", getTableName()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, email);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, first_name, middle_name, last_name, locked, role, email " +
                        "FROM %s WHERE login = ? LIMIT 1", getTableName()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, login);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            return rowSet.next();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, first_name, middle_name, last_name, locked, role, email " +
                        "FROM %s WHERE email = ? LIMIT 1", getTableName()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, email);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            return rowSet.next();
        }
    }

}
