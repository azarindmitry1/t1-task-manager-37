package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.ISessionRepository;
import ru.t1.azarin.tm.api.repository.IUserOwnedRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.ISessionService;
import ru.t1.azarin.tm.model.Session;
import ru.t1.azarin.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull IUserOwnedRepository<Session> getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

}
