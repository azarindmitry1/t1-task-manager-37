package ru.t1.azarin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

import java.sql.SQLException;

public interface IUserService extends IService<User> {

    @NotNull
    @Override
    @SneakyThrows
    User remove(@Nullable User model) throws SQLException;

    @NotNull
    @SneakyThrows
    User removeByLogin(@Nullable String login) throws SQLException;

    @NotNull
    @SneakyThrows
    User removeByEmail(@Nullable String email) throws SQLException;

    @NotNull
    @SneakyThrows
    User create(@Nullable String login, @Nullable String password) throws SQLException;

    @NotNull
    @SneakyThrows
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws SQLException;

    @NotNull
    @SneakyThrows
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    ) throws SQLException;

    @Nullable
    @SneakyThrows
    User findByLogin(@Nullable String login) throws SQLException;

    @Nullable
    @SneakyThrows
    User findByEmail(@Nullable String email) throws SQLException;

    @SneakyThrows
    boolean isLoginExist(@Nullable String login) throws SQLException;

    @SneakyThrows
    boolean isEmailExist(@Nullable String email) throws SQLException;

    @NotNull
    @SneakyThrows
    User setPassword(@Nullable String id, @Nullable String password) throws SQLException;

    @NotNull
    @SneakyThrows
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    ) throws SQLException;

    @SneakyThrows
    void lockUserByLogin(@Nullable String login) throws SQLException;

    @SneakyThrows
    void unlockUserByLogin(@Nullable String login) throws SQLException;

}
