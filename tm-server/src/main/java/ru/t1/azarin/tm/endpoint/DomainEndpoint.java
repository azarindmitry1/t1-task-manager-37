package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.azarin.tm.api.service.IDomainService;
import ru.t1.azarin.tm.api.service.IServiceLocator;
import ru.t1.azarin.tm.dto.request.data.*;
import ru.t1.azarin.tm.dto.response.data.*;
import ru.t1.azarin.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.azarin.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return serviceLocator.getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse backupLoadResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().backupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse backupSaveResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().backupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse base64LoadResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().base64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse base64SaveResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().base64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse binaryLoadResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().binaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse binarySaveResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().binarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXmlResponse jsonLoadFasterXmlResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().jsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxbResponse jsonLoadJaxbResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().jsonLoadJaxb();
        return new DataJsonLoadJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXmlResponse jsonSaveFasterXmlResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().jsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxbResponse jsonSaveJaxbResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().jsonSaveJaxb();
        return new DataJsonSaveJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXmlResponse xmlLoadFasterXmlResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().xmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxbResponse xmlLoadJaxbResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().xmlLoadJaxb();
        return new DataXmlLoadJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXmlResponse xmlSaveFasterXmlResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().xmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxbResponse xmlSaveJaxbResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().xmlSaveJaxb();
        return new DataXmlSaveJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXmlResponse yamlLoadFasterXmlResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().yamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXmlResponse yamlSaveFasterXmlResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().yamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
