package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
