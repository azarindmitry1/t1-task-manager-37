package ru.t1.azarin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    private final String tableName = "tm_task";

    @Override
    protected @NotNull String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.toStatus(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, user_id, project_id, name, description, status, created) " +
                        "VALUES (?, ?, ? , ?, ?, ?, ?)", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getUserId());
            statement.setString(3, task.getProjectId());
            statement.setString(4, task.getName());
            statement.setString(5, task.getDescription());
            statement.setString(6, Status.NOT_STARTED.toString());
            statement.setTimestamp(7, new Timestamp(task.getCreated().getTime()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET project_id = ?, name = ?, description = ?, status = ?, user_id = ? WHERE id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, task.getProjectId());
            preparedStatement.setString(2, task.getName());
            preparedStatement.setString(3, task.getDescription());
            preparedStatement.setString(4, task.getStatus().toString());
            preparedStatement.setString(5, task.getUserId());
            preparedStatement.setString(6, task.getId());
            preparedStatement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT id, user_id, project_id, name, description, status, created "
                + "FROM %s WHERE user_id = ? and project_id = ?", getTableName());
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, projectId);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

}