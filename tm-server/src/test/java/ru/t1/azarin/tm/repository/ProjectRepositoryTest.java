package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.service.ConnectionService;
import ru.t1.azarin.tm.service.PropertyService;
import ru.t1.azarin.tm.util.HashUtil;

import java.sql.SQLException;

import static ru.t1.azarin.tm.constant.ProjectTestData.*;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository(CONNECTION_SERVICE.getConnection());

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION_SERVICE.getConnection());

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws SQLException {
        @NotNull final User user = new User();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() {
        USER_REPOSITORY.removeById(USER_ID);
    }

    @Before
    public void before() throws SQLException {
        PROJECT_REPOSITORY.add(USER_ID, USER1_PROJECT1);
        PROJECT_REPOSITORY.add(USER_ID, USER1_PROJECT2);
    }

    @After
    public void after() {
        PROJECT_REPOSITORY.clear(USER_ID);
    }

    @Test
    public void add() throws SQLException {
        @NotNull final Project project = new Project();
        project.setName("TEST STRING");
        project.setDescription("TEST STRING");
        PROJECT_REPOSITORY.add(USER_ID, project);
        @Nullable final Project createdProject = PROJECT_REPOSITORY.findOneById(USER_ID, project.getId());
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(project.getId(), createdProject.getId());
    }

    @Test
    public void clear() {
        Assert.assertFalse(PROJECT_REPOSITORY.findAll().isEmpty());
        PROJECT_REPOSITORY.clear(USER_ID);
        Assert.assertEquals(0, PROJECT_REPOSITORY.findAll(USER1.getId()).size());
    }

    @Test
    public void existById() {
        Assert.assertTrue(PROJECT_REPOSITORY.existById(USER_ID, USER1_PROJECT1.getId()));
        Assert.assertFalse(PROJECT_REPOSITORY.existById(USER_UNREGISTRY_ID, USER1_PROJECT1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER1_PROJECT_LIST.size(), PROJECT_REPOSITORY.findAll().size());
    }

    @Test
    public void findOneById() {
        @Nullable final Project project = PROJECT_REPOSITORY.findOneById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), project.getId());
    }

    @Test
    public void remove() {
        @Nullable final Project project = PROJECT_REPOSITORY.findOneById(USER_ID, USER1_PROJECT1.getId());
        PROJECT_REPOSITORY.remove(USER_ID, project);
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID, USER1_PROJECT1.getId()));
    }

    @Test
    public void removeById() {
        @Nullable final Project project = PROJECT_REPOSITORY.findOneById(USER_ID, USER1_PROJECT1.getId());
        PROJECT_REPOSITORY.removeById(USER_ID, project.getId());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID, USER1_PROJECT1.getId()));
    }

}
