package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.ITaskService;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.DescriptionEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.NameEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.repository.UserRepository;
import ru.t1.azarin.tm.util.HashUtil;

import java.sql.SQLException;
import java.util.Collections;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private final ITaskService taskService = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION_SERVICE.getConnection());

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @BeforeClass
    public static void setUp() throws SQLException {
        @NotNull final User user = new User();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        @Nullable final User user = USER_REPOSITORY.findByLogin(USER1_LOGIN);
        USER_REPOSITORY.remove(user);
    }

    @Before
    public void before() throws SQLException {
        taskService.add(USER_ID, USER1_TASK1);
    }

    @After
    public void after() {
        taskService.clear(USER_ID);
    }

    @Test
    public void add() throws SQLException {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(emptyString, new Task()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(nullString, new Task()));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.add(USER_ID, null));
        taskService.clear(USER_ID);
        Assert.assertEquals(0, taskService.findAll(USER_ID).size());
        taskService.add(USER_ID, new Task());
        Assert.assertEquals(1, taskService.findAll(USER_ID).size());
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(
                emptyString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(
                nullString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(
                USER_ID, emptyString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(
                USER_ID, nullString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(
                USER_ID, USER1_TASK1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(
                USER_ID, USER1_TASK1.getName(), nullString)
        );
        @Nullable final Task task = taskService.create(USER_ID, testString, testString);
        Assert.assertEquals(testString, task.getName());
        Assert.assertEquals(testString, task.getDescription());
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(nullString));
        taskService.clear(USER_ID);
        Assert.assertEquals(0, taskService.findAll(USER_ID).size());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(
                emptyString, USER1_TASK1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(
                nullString, USER1_TASK1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(
                USER_ID, emptyString, Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(
                USER_ID, nullString, Status.COMPLETED)
        );
        @Nullable final Task task = taskService.changeTaskStatusById(USER_ID, USER1_TASK1.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void existById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existById(USER_ID, nullString));
        @Nullable final Task task = taskService.create(USER_ID, testString, testString);
        Assert.assertTrue(taskService.existById(USER_ID, task.getId()));
        Assert.assertFalse(taskService.existById(USER_UNREGISTRY_ID, USER1_TASK1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(nullString));
        Assert.assertEquals(1, taskService.findAll(USER_ID).size());
        Assert.assertEquals(0, taskService.findAll(USER_UNREGISTRY_ID).size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID, nullString));
        @Nullable final Task task = taskService.findOneById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(USER1_TASK1.getId(), task.getId());
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(
                emptyString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(
                nullString, USER1_PROJECT1.getId())
        );
        Assert.assertEquals(taskService.findAllByProjectId(USER_ID, nullString), Collections.emptyList());
    }

    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(emptyString, USER1_TASK1));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(nullString, USER1_TASK1));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.remove(USER_ID, null));
        @Nullable final Task task = taskService.findOneById(USER_ID, USER1_TASK1.getId());
        taskService.remove(USER_ID, task);
        Assert.assertEquals(0, taskService.findAll(USER_ID).size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID, nullString));
        taskService.removeById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(0, taskService.findAll(USER_ID).size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(
                emptyString, USER1_TASK1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(
                nullString, USER1_TASK1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(
                USER_ID, emptyString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(
                USER_ID, nullString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(
                USER_ID, USER1_TASK1.getId(), emptyString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(
                USER_ID, USER1_TASK1.getId(), nullString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(
                USER_ID, USER1_TASK1.getId(), USER1_TASK1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(
                USER_ID, USER1_TASK1.getId(), USER1_TASK1.getName(), nullString)
        );
        @Nullable final Task task = taskService.updateById(
                USER_ID, USER1_TASK1.getId(), testString, USER1_TASK1.getDescription()
        );
        Assert.assertEquals(testString, task.getName());
    }

}
