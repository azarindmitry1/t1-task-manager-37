package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.IProjectService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.DescriptionEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.NameEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.repository.UserRepository;
import ru.t1.azarin.tm.util.HashUtil;

import java.sql.SQLException;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT2;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private final IProjectService projectService = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION_SERVICE.getConnection());

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "TEST_STRING";

    @BeforeClass
    public static void setUp() throws SQLException {
        @NotNull final User user = new User();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        @Nullable final User user = USER_REPOSITORY.findByLogin(USER1_LOGIN);
        USER_REPOSITORY.remove(user);
    }

    @Before
    public void before() throws SQLException {
        projectService.add(USER_ID, USER1_PROJECT1);
        projectService.add(USER_ID, USER1_PROJECT2);
    }

    @After
    public void after() {
        projectService.clear(USER_ID);
    }

    @Test
    public void add() throws SQLException {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(emptyString, new Project()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(nullString, new Project()));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.add(USER_ID, null));
        projectService.clear(USER_ID);
        Assert.assertEquals(0, projectService.findAll(USER_ID).size());
        projectService.add(USER_ID, new Project(testString, testString));
        Assert.assertEquals(1, projectService.findAll(USER_ID).size());
    }

    @Test
    public void create() throws SQLException {
        projectService.remove(USER1_PROJECT1);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(
                emptyString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(
                nullString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(
                USER_ID, emptyString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(
                USER_ID, nullString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(
                USER_ID, USER1_PROJECT1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(
                USER_ID, USER1_PROJECT1.getName(), nullString)
        );
        @Nullable final Project project = projectService.create(
                USER_ID, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()
        );
        Assert.assertEquals(USER1_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER1_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER1_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(nullString));
        projectService.clear(USER_ID);
        Assert.assertEquals(0, projectService.findAll(USER_ID).size());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(
                emptyString, USER1_PROJECT1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(
                nullString, USER1_PROJECT1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(
                USER_ID, emptyString, Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(
                USER_ID, nullString, Status.COMPLETED)
        );
        @Nullable final Project project = projectService.changeProjectStatusById(
                USER_ID, USER1_PROJECT1.getId(), Status.COMPLETED
        );
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void existById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existById(USER_ID, nullString));
        Assert.assertTrue(projectService.existById(USER_ID, USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existById(USER_UNREGISTRY_ID, USER1_PROJECT1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(nullString));
        Assert.assertEquals(0, projectService.findAll(USER_UNREGISTRY_ID).size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID, nullString));
        @Nullable final Project project = projectService.findOneById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), project.getId());
    }

    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(emptyString, USER1_PROJECT1));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(nullString, USER1_PROJECT1));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.remove(USER_ID, null));
        projectService.remove(USER_ID, USER1_PROJECT1);
        Assert.assertEquals(1, projectService.findAll(USER_ID).size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID, nullString));
        projectService.removeById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertEquals(1, projectService.findAll(USER_ID).size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(
                emptyString, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(
                nullString, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(
                USER_ID, emptyString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(
                USER_ID, nullString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(
                USER_ID, USER1_PROJECT1.getId(), emptyString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(
                USER_ID, USER1_PROJECT1.getId(), nullString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(
                USER_ID, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(
                USER_ID, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), nullString)
        );
        @Nullable final Project project = projectService.updateById(USER_ID, USER1_PROJECT1.getId(), testString, testString);
        Assert.assertEquals(testString, project.getName());
    }

}
