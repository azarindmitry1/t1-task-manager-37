package ru.t1.azarin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.azarin.tm.dto.response.system.ApplicationVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public final static String NAME = "version";

    @NotNull
    public final static String ARGUMENT = "-v";

    @NotNull
    public final static String DESCRIPTION = "Display application version.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
