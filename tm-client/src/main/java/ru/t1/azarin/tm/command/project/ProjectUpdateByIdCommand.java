package ru.t1.azarin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "project-update-by-id";

    @NotNull
    public final static String DESCRIPTION = "Update project by id.";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().updateByIdResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
