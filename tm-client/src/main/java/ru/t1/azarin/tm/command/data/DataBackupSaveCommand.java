package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataBackupSaveRequest;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "backup-save";

    @NotNull
    public final static String DESCRIPTION = "Save backup data to file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(getToken());
        getDomainEndpoint().backupSaveResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
