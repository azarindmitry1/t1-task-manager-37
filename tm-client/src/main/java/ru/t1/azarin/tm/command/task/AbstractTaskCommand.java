package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    protected List<Task> renderTask(@NotNull final List<Task> tasks) {
        int index = 1;
        for (@NotNull Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        return tasks;
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}
