package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataJsonLoadJaxbRequest;

public final class DataJsonLoadJaxbCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-load-json-jaxb";

    @NotNull
    public final static String DESCRIPTION = "Load data from json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadJaxbRequest request = new DataJsonLoadJaxbRequest(getToken());
        getDomainEndpoint().jsonLoadJaxbResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
