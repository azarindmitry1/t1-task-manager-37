package ru.t1.azarin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.user.UserRegistryRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-registry";

    @NotNull
    public final static String DESCRIPTION = "Registry user.";

    @Override
    public void execute() throws SQLException {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @NotNull final User user = getUserEndpoint().registryResponse(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
